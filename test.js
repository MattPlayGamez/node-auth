const calc = require('basic-calculator-js')
const prompt = require('prompt-sync')()
const teken = prompt('what do you want to do: divide, multiply or subtract: ')
const cijfer1 = prompt(`Which number you are gonna ${teken} first: `)
const cijfer2 = prompt(`Which number you gonna ${teken} with ${cijfer1}: `)

if ( teken === 'divide') {
    const answer = calc.divide(cijfer1,cijfer2)
    console.log(answer)
}
if ( teken === 'multiply') {
    const answer = calc.multiply(cijfer1,cijfer2)
    console.log(answer)
}
if ( teken === 'subtract') {
    const answer = calc.subtract(cijfer1,cijfer2)
    console.log(answer)
}